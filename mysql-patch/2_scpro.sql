/*
Module Name: Scpro Module
Changed By: Prasoon Saxena
Date: 19-7-2016
Comment:  Create new table '`scpro_class_subject_template`'

*/


CREATE TABLE `scpro_class_subject_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

--//@UNDO

DROP TABLE `scpro_class_subject_template`;

