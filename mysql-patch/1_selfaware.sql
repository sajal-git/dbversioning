CREATE TABLE `marks_category_mapp` (
  `category_map_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `school_id` int(11) DEFAULT NULL,
  `AYID` int(11) DEFAULT NULL,
  `is_grade` int(1) DEFAULT '0',
  `modify_by` int(11) DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`category_map_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


ALTER TABLE `eol_prod`.`grade_details`
ADD COLUMN `grade_point` VARCHAR(45) NULL DEFAULT NULL COMMENT '' AFTER `pass_level`;


CREATE TABLE `eol_prod`.`exam_pattern_mapping` (
  `pattern_map_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `exam_pattern_id` SMALLINT NOT NULL COMMENT '',
  `title` VARCHAR(45) NOT NULL COMMENT '',
  `board_id` SMALLINT NOT NULL COMMENT '',
  `level_id` SMALLINT NOT NULL COMMENT '',
  `school_id` SMALLINT NULL COMMENT '',
  `AYID` SMALLINT NULL COMMENT '',
  `updated_by` VARCHAR(45) NULL COMMENT '',
  `created_date` DATETIME NULL COMMENT '',
  `modified_date` DATETIME NULL COMMENT '',
  PRIMARY KEY (`pattern_map_id`)  COMMENT '');


/* Self-Awareness*/
ALTER TABLE `eol_prod`.`student_self_awareness`
ADD COLUMN `weakness` VARCHAR(255) NULL DEFAULT NULL COMMENT '' AFTER `created_date`;

